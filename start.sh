#!/bin/bash

set -eu

readonly mysql_cli="mysql -u${MYSQL_USERNAME} -p${MYSQL_PASSWORD} -h ${MYSQL_HOST} -P ${MYSQL_PORT} ${MYSQL_DATABASE}"

echo "=> Ensure directories"
mkdir -p /app/data/ /run/prestashop/sessions /tmp/logs

if [[ ! -f /app/data/.installed ]]; then
    echo "=> Initial copy"
    cp -rf /app/code/* /app/data/

    echo "=> Run cli installer"
    cd /app/data/install-dev
    php index_cli.php --domain=$APP_DOMAIN --db_server=$MYSQL_HOST --db_user=$MYSQL_USERNAME --db_password=$MYSQL_PASSWORD --db_name=$MYSQL_DATABASE --password=changeme --email=admin@cloudron.io
    cd ..

    rm -rf /app/data/install-dev

    # This value does not exist initially but needs to be set
    $mysql_cli -e "INSERT INTO ps_configuration SET name='PS_SSL_ENABLED_EVERYWHERE',value='1',date_add=CURRENT_TIMESTAMP,date_upd=CURRENT_TIMESTAMP;"

    touch /app/data/.installed
fi

echo "=> Configure settings"
$mysql_cli -e "UPDATE ps_configuration SET value='1' WHERE name='PS_SSL_ENABLED_EVERYWHERE';"
$mysql_cli -e "UPDATE ps_configuration SET value='1' WHERE name='PS_SSL_ENABLED';"
$mysql_cli -e "UPDATE ps_configuration SET value='${APP_DOMAIN}' WHERE name='PS_SHOP_DOMAIN_SSL';"
$mysql_cli -e "UPDATE ps_configuration SET value='${APP_DOMAIN}' WHERE name='PS_SHOP_DOMAIN';"

# enable css and javascript cache
$mysql_cli -e "UPDATE ps_configuration SET value='1' WHERE name='PS_CSS_THEME_CACHE';"
$mysql_cli -e "UPDATE ps_configuration SET value='1' WHERE name='PS_JS_THEME_CACHE';"

# magic value "2" to indicate not to use php mail() but the settings below directy
$mysql_cli -e "UPDATE ps_configuration SET value='2' WHERE name='PS_MAIL_METHOD';"
$mysql_cli -e "UPDATE ps_configuration SET value='0' WHERE name='PS_MAIL_SMTP_ENCRYPTION';"
$mysql_cli -e "UPDATE ps_configuration SET value='${MAIL_SMTP_PORT}' WHERE name='PS_MAIL_SMTP_PORT';"
$mysql_cli -e "UPDATE ps_configuration SET value='${MAIL_SMTP_PASSWORD}' WHERE name='PS_MAIL_PASSWD';"
$mysql_cli -e "UPDATE ps_configuration SET value='${MAIL_SMTP_USERNAME}' WHERE name='PS_MAIL_USER';"
$mysql_cli -e "UPDATE ps_configuration SET value='${MAIL_SMTP_SERVER}' WHERE name='PS_MAIL_SERVER';"
$mysql_cli -e "UPDATE ps_configuration SET value='${MAIL_FROM}' WHERE name='PS_SHOP_EMAIL';"
$mysql_cli -e "UPDATE ps_configuration SET value='${MAIL_DOMAIN}' WHERE name='PS_MAIL_DOMAIN';"

echo "=> Configure parameters.php"
sed -e "s/'database_host' => '.*',/'database_host' => '${MYSQL_HOST}',/" \
    -e "s/'database_port' => '.*',/'database_port' => '${MYSQL_PORT}',/" \
    -e "s/'database_name' => '.*',/'database_name' => '${MYSQL_DATABASE}',/" \
    -e "s/'database_user' => '.*',/'database_user' => '${MYSQL_USERNAME}',/" \
    -e "s/'database_password' => '.*',/'database_password' => '${MYSQL_PASSWORD}',/" \
    -e "s/'mailer_host' => '.*',/'mailer_host' => '${MAIL_SMTP_SERVER}',/" \
    -e "s/'mailer_user' => .*,/'mailer_user' => '${MAIL_SMTP_USERNAME}',/" \
    -e "s/'mailer_password' => .*,/'mailer_password' => '${MAIL_SMTP_PASSWORD}',/" \
    -e "s/'ps_caching' => '.*',/'ps_caching' => 'CacheMemcached',/" \
    -e "s/'ps_cache_enable' => .*,/'ps_cache_enable' => true,/" \
    -i /app/data/app/config/parameters.php

echo "=> Ensure directory permissions"
chown -R www-data.www-data /app/data /run/prestashop/

echo "=> Starting supervisor"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i PrestaShop
