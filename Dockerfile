FROM cloudron/base:0.10.0
MAINTAINER Authors name <support@cloudron.io>

RUN mkdir -p /app/code
WORKDIR /app/code

ENV PATH /usr/local/node-4.7.3/bin:$PATH

RUN apt-get update && apt-get install -y composer php-curl php-mcrypt php-gd php-zip php-dom php-soap memcached php-memcached && rm -r /var/cache/apt /var/lib/apt/lists

RUN curl -L https://github.com/PrestaShop/PrestaShop/archive/1.7.1.2.tar.gz | tar -xz --strip-components 1 -f -
RUN composer install

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
RUN sed -e "s,MaxSpareServers[^:].*,MaxSpareServers 5," -i /etc/apache2/mods-available/mpm_prefork.conf
RUN a2disconf other-vhosts-access-log && a2enmod rewrite
RUN echo "Listen 80" > /etc/apache2/ports.conf
RUN ln -sf /etc/apache2/sites-available/prestashop.conf /etc/apache2/sites-enabled/prestashop.conf
COPY apache2-prestashop.conf /etc/apache2/sites-available/prestashop.conf

# configure php
RUN crudini --set /etc/php/7.0/apache2/php.ini PHP upload_max_filesize 64M && \
    crudini --set /etc/php/7.0/apache2/php.ini PHP post_max_size 64M && \
    crudini --set /etc/php/7.0/apache2/php.ini PHP memory_limit 64M && \
    crudini --set /etc/php/7.0/apache2/php.ini PHP max_execution_time 50 && \
    crudini --set /etc/php/7.0/apache2/php.ini Session session.save_path /run/prestashop/sessions && \
    crudini --set /etc/php/7.0/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/7.0/apache2/php.ini Session session.gc_divisor 100 && \
    crudini --set /etc/php/7.0/cli/php.ini Session session.save_path /run/prestashop/sessions   # for the cli installer

# configure supervisor
RUN sed -e 's,^logfile=.*$,logfile=/run/prestashop/supervisord.log,' -i /etc/supervisor/supervisord.conf
COPY supervisor/ /etc/supervisor/conf.d/

COPY initial_htaccess /app/code/.htaccess
COPY start.sh /app/

RUN chown -R www-data.www-data /app/code

WORKDIR /app/data

CMD [ "/app/start.sh" ]
